<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/compositions/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A

	// B
	'bouton_sortie'                    => "Revenir à la liste des sites",

	// C
	'cfg_titre_parametrages'           => "Paramétrages du plugin Evaluation de sites",

	// E
	'erreur_aucune_evaluation_trouvee' => "Aucune évaluation trouvée.",
	'erreur_lien_rubrique_evaluation'  => "Aucune évaluation n'est pour l'instant liée à la rubrique @id_rubrique@.",

	// I
	'info_chargement_en_cours'         => "Le site @site@ est en cours de chargement,<br />merci de bien vouloir patienter...",

	// S
	'sites_critique'                   => "Sites évalués",

	// L
	'label_lien_direct'                => "visiter",
	'label_aucune_eval'                => "Aucun avis.",
	'label_evaluations'                => "avis.",
	'label_a_vote_oui'                 => "a voté",
	'label_a_vote_non'                 => "voter !",
	'label_categorie'                  => "Catégorie",

	// T
	'title_aucun_avis'                 => "Aucun avis pour l'instant. Visiter le site et donner votre avis",
	'title_donner_votre_avis'          => "Visiter le site et donner votre avis",
	'title_modifier_votre_avis'        => "Visiter le site à nouveau, et modifier ou compléter votre évaluation ",
	'title_voir_les_avis'              => "Voir tous les avis",
	'title_voir_en_direct'             => "Visiter le site en direct, sans formulaire d'évaluation.",
);

?>