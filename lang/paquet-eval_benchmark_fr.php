<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/compositions/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
    // R
    'eval_benchmark_nom'         => "Critique de sites - Benchmarks",
    'eval_benchmark_slogan'      => "Permet d'évaluer des objets SPIP de type site web",
    'eval_benchmark_description' => "Ce plugin s'utilise avec le plugin Evaluations; il propose une composition de rubrique listant tous les sites d'une branche.",

);

?>
